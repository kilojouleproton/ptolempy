from scipy.spatial import Voronoi
import numpy as np

class BaseGrid(object):
	def __init__(self, *args, **kwargs):
		self.points = []
		self._coords = {}
		self.coordlim = ()
	def get_coord(self, coordtup):
		return self.points[self._coords[coordtup]]
	def get_point(self, index): return self.points.__getitem__(index)

class HexGrid2D(BaseGrid):
	def __init__(self, *args, **kwargs):
		''' constructor for 2D hex grids
		Parameters
		----------
		width : numeric
			Number of columns in grid.
		height : numeric
			Number of rows in grid. 
		horizontal : boolean, optional
			Use horizontal tiling (pointy side up/down) instead of vertical tiling.
		xspacing : float, optional
			Change the horizontal spacing between hexes in the same row. Defaults to 2
		yspacing : float, optional
			Change the vertical spacing between hexes in the same column. Defaults to 1
		'''
		super().__init__(self, *args, **kwargs)
		self.width = kwargs['width'] if 'width' in kwargs else args[0]
		self.height = kwargs['height'] if 'height' in kwargs else args[1]
		self.horizontal = kwargs.get('horizontal', False)
		self.xspacing = kwargs.get('xspacing', 2)
		self.yspacing = kwargs.get('yspacing', 1)
		
		self.points = None
		row = 0
		index = 0
		for y in np.arange(0, self.height + self.yspacing, self.yspacing):
			if not (0 <= y <= self.height): continue
			col = 0
			for x in np.arange(0, self.width + self.xspacing, self.xspacing):
				if not (0 <= (x + (row%2) * self.xspacing/2) <= self.width): continue
				if self.points is None: self.points = np.array([x + (row%2) * self.xspacing/2, y])
				else: self.points = np.vstack([self.points, np.array([x + (row%2) * self.xspacing/2, y])])
				self._coords[(col, row)] = index
				index += 1
				col += 1
			row += 1

class BaseTile(object):
	''' Convex tiles. Concave tiles can theoretically be modeled with this class but may lead to, pun intended, myriad corner cases. 

	Parameters
	----------
	
	'''
	#TODO: Implement a ConcaveTile class. Priority changes: is_neighbor
	def __init__(self, *args, **kwargs):
		if len(args) or 'template' in kwargs:
			template = args[0] if len(args) else kwargs['template']
			self.coord = template.coord
			self.pos = template.pos
			self.vertices = template.vertices
			self.poly = template.poly
			self.wholemap = template.wholemap
			
		self.coord = kwargs.get('coord', None)
		self.pos = kwargs.get('pos', None)
		self.vertices = kwargs.get('vertices', [])
		self.poly = kwargs.get('poly', None)
		self.wholemap = kwargs.get('wholemap', None)

	def is_neighbor(self, other):
		''' Check if tile shares an edge with another tile. Using sets for speed here renders this method unreliable for concave tiles, especially those in irregular grids. '''
		if set(self.vertices).intersection(set(other.vertices)) >= 2: return True
		else: return False

	def is_diagonal(self, other):
		''' Check if tile intersects another at a vertex '''
		pass

class BaseMap(object):
	''' A heavy class for building maps from tiles

	Note: BaseMap does not by itself provide any means of checking for adjacency. Use VoronoiMap or GridMap instead.'''
	def __init__(self, *args, **kwargs):
		'''
		Parameters
		----------
		grid : BaseGrid or BaseGrid-derived instance
			Contains points and coordinates for the map.
		tiletype : BaseTile or BaseTile-derived prototype
			Provides a template for map tiles.
		'''
		#super().__init__(*args, **kwargs)
		self.tiletype = BaseTile

	def set_tile(self, prototype):
		self.tiletype = prototype
	def _update_tiles(self):
		pass

class VoronoiMap(BaseMap):
	''' A map type characterized by arbitrary convex tiles '''
	#TODO: Figure out wrapping
	def __init__(self, *args, **kwargs):
		'''
		Parameters
		----------
		grid : BaseGrid or BaseGrid-derived instance
			Supplies points for the Voronoi step.
		tiletype : BaseTile or BaseTile-derived prototype
			Provides a template for map tiles.'''
		super().__init__(*args, **kwargs)
