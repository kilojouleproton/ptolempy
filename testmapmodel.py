import unittest

import mapmodel
import matplotlib.pyplot as plt
import numpy as np

class TestGrid(unittest.TestCase):
	def test_hexgrid(self):
		grid = mapmodel.HexGrid2D(10, 10)
		assert grid.points.shape == (61, 2)
		assert tuple(grid.get_coord((2,4))) == tuple((np.array([4.,4.])))

		#grid = mapmodel.HexGrid2D(20, 20, xspacing=3.46)
		#plt.scatter(grid.points[:,0], grid.points[:,1])
		#plt.show()

class TestHexMap(unittest.TestCase):
	hexgrid = mapmodel.HexGrid2D(20, 20)

	def test_init(self):
		model = mapmodel.BaseMap(self.hexgrid.points)
	def test_voronoi(self):
		pass

if __name__ == '__main__': unittest.main()
